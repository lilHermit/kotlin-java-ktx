# ChangeLog

- 1.0.0
  - Initial release
- 1.0.1
  - Added getter to Dates.today, Dates.tomorrow and Dates.yesterday so the values are not fixed (static)